package hust.soict.globalict.aims.utils;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

public class MyDate {
	private int day, month, year;

	public MyDate() {
		LocalDate currentDate = LocalDate.now();
		this.day = currentDate.getDayOfMonth();
		this.month = currentDate.getMonthValue();
		this.year = currentDate.getYear();
	}
	
	public MyDate(int day, int month, int year)
	{
		//super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate (String day, String month, String year)
	{
		String[] unitStrings = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
		String[] units_ordinal = {"", "first", "second", "third", "forth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth"}; 		String[] tensStrings = {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
		String[] scales = {"", "", "hundred", "thousand"};
		String[] months = {"", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		//set day
		this.day = 0;
		if (day.contains(" ")) {
			String[] partStrings = day.split(" ");
			traverse:
			for (String e : partStrings) {
				for (int i=0; i<units_ordinal.length; i++) {
					if (e.equalsIgnoreCase(units_ordinal[i])) {
						this.day += i;
						continue traverse;
					}
				}
				for (int i=2; i<tensStrings.length; i++) {
					if (e.equalsIgnoreCase(tensStrings[i])) {
						this.day += i*10;
						continue traverse;
					}
				}
			}
		}
		else {
			if (day.equalsIgnoreCase(tensStrings[2])) this.day = 20;
			else if (day.equalsIgnoreCase(tensStrings[3])) this.day = 30;
			else {
				for (int i=0; i<unitStrings.length; i++) 
					if (day.equalsIgnoreCase(units_ordinal[i]))
						this.day = i;
			}
		}
		
		//set month 
		for (int i=1; i<months.length; i++) {
			if (month.equalsIgnoreCase(months[i])) {
				this.month = i;
				break;
			}
		}
		
		//set year
		this.year = 0;
		if (year.contains(" ")) {
			String[] partStrings = year.split(" ");
			int ans = 0;
			traverse:
			for (String e: partStrings) {
				if (e.equalsIgnoreCase("and")) 
					continue traverse;
				for (int i=0; i<unitStrings.length; i++) {
					if (e.equalsIgnoreCase(unitStrings[i])) {
						this.year += i;
						ans = i;
						continue traverse;
					}
				}
				for (int i=0; i<tensStrings.length; i++) {
					if (e.equalsIgnoreCase(tensStrings[i])) {
						this.year += i*10;
						continue traverse;
					}
				}
				for (int i=2; i<scales.length; i++) {
					if (e.equalsIgnoreCase(scales[i])) {
						this.year *= (int) Math.pow(10,  i);
						continue traverse;
					}
				}
			}
		}
		
	}
	
	public MyDate (String date) 
	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd yyyy", Locale.ENGLISH);
		LocalDate currentDate = LocalDate.parse(date, formatter);
		this.day = currentDate.getDayOfMonth();
		this.month = currentDate.getMonthValue();
		this.year = currentDate.getYear();
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (!(day>0 && day<32)) 
			this.day = day;
		else System.out.println("Invalid!\nThe day must be between 1 and 31!");
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (!(month>0 && month<13)) 
			this.month = month;
		else System.out.println("Invalid!\nThe month must be between 1 and 12!");
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate accept()
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a date (Eg: Jun 01 2022)");
		String dateString = keyboard.nextLine();
		MyDate date = new MyDate(dateString);
		keyboard.close();
		return date;
	}
	
	public void print() 
	{
		//Insert the prefix after the date
		String monthString = new DateFormatSymbols().getMonths()[month-1];
		switch (day) {
		case 1: {
			System.out.print( monthString + " " + day + "st " + year );
			break;
		}
		case 2: {
			System.out.print(monthString + " " + day + "nd " + year );
			break;
		}
		case 3: {
			System.out.print(monthString + " " + day + "rd " + year );
			break;
		}
		
		default:
			System.out.print(monthString + " " + day + "th " + year );
			break;
		}
	}
	
	/*public void anotherPrintFormat()
	{
		String string = this.day+"/"+this.month+"/"+this.year;
		  
		Date date1;
		try {
			date1 = new SimpleDateFormat("dd/MM/YYYY").parse(string);
			SimpleDateFormat DateFor = new SimpleDateFormat("dd-MMM-yyyy");
			String stringDate = DateFor.format(date1);
			System.out.println(stringDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}*/
	
	/*public void print(int format) {
		String dateFormat;
		switch(format) {
			case 1:
				dateFormat = this.day + " / " + this.month + " / " + this.year;
				System.out.println("dd/mm/yyyy: " + dateFormat);
				break;

			case 2:
				dateFormat = this.year%100 + " / " + this.month + " / " + this.day;
				System.out.println("yy/mm/dd: " + dateFormat);
				break;
				
			default:
				System.out.println("ERROR!");
				break;
		}
	}*/


}
