package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{

	//private String title;
	//private String category;
	//private float cost;
	private List<String> authors = new ArrayList<String>();
	
	Book(String title){
		super(title);
	}
	
	Book(String title, String category){
		super(title, category);
	}
	
	Book (String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	
	public Book(String id, String title, String category, float cost, List<String> authors) {
        super(id, title, category, cost);
        //this.title = title;
        //this.category = category;
        //this.cost = cost;
        this.authors = authors;
    }

    public Book(Book book) {
        super(book.getId(), book.getTitle(), book.getCategory(), book.getCost());
        this.authors = book.getAuthors();
    }
    
	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	//public String getCategory() {
		//return category;
	//}

	//public void setCategory(String category) {
		//this.category = category;
	//}

	//public float getCost() {
	//	return cost;
	//}

	//public void setCost(float cost) {
	//	this.cost = cost;
	//}

	//public String getTitle() {
	//	return title;
	//}

	//public void setTitle(String title) {
	//	this.title = title;
	//}

	public void addAuthor (String authorName)
	{
		for (String author: this.authors) {
			if (author.equals(authorName)) {
				System.out.println("This author has already been in the authors list.\nCannot add!");
				return;
			}
		}
		this.authors.add(authorName);
		System.out.println("Author " + authorName + " is added!");
	}
	
	public void removeAuthor (String authorName)
	{
		int flag = 0;
		for (String author: this.authors) {
			if (author.equals(authorName)) {
				System.out.println("Author " + authorName + " is removed!");
				this.authors.remove(authorName);
				flag = 1;
			}
		}
		if (flag == 0) System.out.println("Cannot find author " + authorName + " in this list!");
	}
}
