package hust.soict.globalict.aims.media;

public class Media {

	private String title;
	private String category;
	private float cost;
	private String id;
	
	public Media(String title) {
		this.title = title;
	}

	public Media (String title, String category) {
		this(title);
		this.category = category;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
	
	public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
	public Media(String id, String title, String category, float cost) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.cost = cost;
    }
}
