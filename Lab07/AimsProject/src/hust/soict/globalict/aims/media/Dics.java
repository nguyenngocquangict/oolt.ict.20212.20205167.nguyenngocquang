package hust.soict.globalict.aims.media;

public class Dics extends Media {

	private int length;
	private String director;

	public Dics (String title, String category, float cost, int length) {
        super(title, category, cost);
        this.length = length;
    }

	public Dics (String title, String category, float cost, int length, String director) {
        super(title, category, cost);
        this.director = director;
        this.length = length;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDirector() {
        return this.director;
    }

    public void setDirector(String director) {
        this.director = director;
    }
}

