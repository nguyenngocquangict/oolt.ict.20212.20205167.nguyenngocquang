package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{

	private List<String> authors = new ArrayList<String>();
	
	//public Book(String title){
		//super(title);
	//}
	
	//public Book(String title, String category){
		//super(title, category);
	//}
	
	public Book (String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	
	public Book(String title, String category, float cost, List<String> authors) {
        super(title, category, cost);
        this.authors = authors;
    }

    public Book(Book book) {
        super(book.getTitle(), book.getCategory(), book.getCost());
        this.authors = book.getAuthors();
    }
    
	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	

	//public void setCategory(String category) {
		//this.category = category;
	//}

	//public float getCost() {
	//	return cost;
	//}

	//public void setCost(float cost) {
	//	this.cost = cost;
	//}

	//public String getTitle() {
	//	return title;
	//}

	//public void setTitle(String title) {
	//	this.title = title;
	//}

	public void addAuthor (String authorName)
	{
		for (String author: this.authors) {
			if (author.equals(authorName)) {
				System.out.println("This author has already been in the authors list.\nCannot add!");
				return;
			}
		}
		this.authors.add(authorName);
		System.out.println("Author " + authorName + " is added!");
	}
	
	public void removeAuthor (String authorName)
	{
		int flag = 0;
		for (String author: this.authors) {
			if (author.equals(authorName)) {
				System.out.println("Author " + authorName + " is removed!");
				this.authors.remove(authorName);
				flag = 1;
			}
		}
		if (flag == 0) System.out.println("Cannot find author " + authorName + " in this list!");
	}
}
