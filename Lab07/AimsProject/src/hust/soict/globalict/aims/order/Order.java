package hust.soict.globalict.aims.order;
import java.util.ArrayList;
import java.util.Random;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	
	//private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	public int qtyOrdered = 0;
	private MyDate dateOrdered;
	private static int nbOrders = 0;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	
	public static int getNumberOfOrders() {
        return nbOrders;
    }
    
    public int getQtyOrdered () {
    	return this.qtyOrdered;
    }
    
    public Media getMedia(int index) {
        return itemsOrdered.get(index);
    }


	public Order ()
	{
		if (nbOrders < MAX_LIMITTED_ORDERS) {
			//qtyOrdered = 0;
			dateOrdered = new MyDate();
			nbOrders = nbOrders + 1;
		}
		else {
			System.out.println("Exceed the number of orders!");
			nbOrders = nbOrders + 1;
		}
	}
	/*public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (nbOrders <= MAX_LIMITTED_ORDERS) {
			if (qtyOrdered < MAX_NUMBERS_ORDERED) {
				itemOrdered[qtyOrdered] = disc;
				qtyOrdered = qtyOrdered + 1;
				System.out.print("This disc has been added!\n");
			}
			else System.out.print("The order is almost full!\n");
		}
	}
	
	public void addDigitalVideoDics(DigitalVideoDisc [] dvdList)
	{
		if (nbOrders <= MAX_LIMITTED_ORDERS) {
			if (dvdList.length + qtyOrdered >= MAX_NUMBERS_ORDERED) {
				System.out.print("The list of disc cannot be added because of full ordered items!\n");
			}
			else {
				for (int i=0; i<dvdList.length; i++) {
					itemOrdered[qtyOrdered] = dvdList[i];
					qtyOrdered = qtyOrdered+1;
				}
				System.out.print("The list of disc have been added!\n");
			}
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
	{
		if (nbOrders <= MAX_LIMITTED_ORDERS) {
			if (qtyOrdered < MAX_NUMBERS_ORDERED) {
				itemOrdered[qtyOrdered] = dvd1;
				qtyOrdered = qtyOrdered + 1;
				System.out.print("The first dvd has been added!\n");
			}
			else System.out.print("The order is almost full!\n");
			if (qtyOrdered < MAX_NUMBERS_ORDERED) {
				itemOrdered[qtyOrdered] = dvd2;
				qtyOrdered = qtyOrdered + 1;
				System.out.print("The second dvd has been added!\n");
			}
			else System.out.print("The order is almost full!\n");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if (nbOrders <= MAX_LIMITTED_ORDERS) {
			if (itemOrdered[qtyOrdered-1] == disc) {
				--qtyOrdered;
				System.out.print("This disc has been removed\n");
				return;
			}
			if (qtyOrdered > 0) {
				for (int i=0; i<qtyOrdered; i++) 
					if (itemOrdered[i] == disc) {
						itemOrdered[i] = itemOrdered[--qtyOrdered]; 
						System.out.print("This disc has been removed\n");
						return;
					}
			}
			System.out.print("This disc does not have in your order\n");
		}
	}*/
	
	
	public void addMedia(Media media) {
		if (qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemsOrdered.add(media);
			qtyOrdered++;
			System.out.println("The new media has been added successfully!");
		} else {
			System.out.println("This order is already full");
		}
	}
	
	//delete by reference
	public void removeMedia (Media media)  {
		if (qtyOrdered == 0) {
            System.out.println("Empty media list!");
        } else {
            itemsOrdered.remove(media);
            qtyOrdered--;
            System.out.println("Removed [" + media.getTitle() + "] successfully!");
        }
    }
    
    //delete by index
    public void removeMedia(int index) {
        if (qtyOrdered == 0) {
            System.out.println("Empty media list!");
        } else {
            itemsOrdered.remove(itemsOrdered.get(index));
            qtyOrdered--;
            System.out.println("Removed [" + index + "] successfully!");
        }
    }

    /*public void removeMedia(String id) {
        if (qtyOrdered == 0) {
            System.out.println("Empty media list!");
        } else {
            int checkflag = 0;
            for (Media item: itemsOrdered) {
                String comID = new String(item.getId());
                if(id.equals(comID)) {
                    itemsOrdered.remove(item);
                    qtyOrdered--;
                    checkflag = 1;
                    break;
                }
            }
            
            if (checkflag == 1) System.out.println("Removed [" + id + "] successfully!");
            else System.out.println("Cannot find the media with the ID [" + id + "] in this order list!" );
        }
    }*/
    
    /*public void removeMedia(String id) {
        if (qtyOrdered == 0) {
            System.out.println("Empty media list!");
        } else {
            int checkflag = 0;
            for (Media item: itemsOrdered) {
                String comID = new String(item.getId());
                if(id.equals(comID)) {
                    itemsOrdered.remove(item);
                    qtyOrdered--;
                    checkflag = 1;
                    break;
                }
            }
            
            if (checkflag == 1) System.out.println("Removed [" + id + "] successfully!");
            else System.out.println("Cannot find the media with the ID [" + id + "] in this order list!" );
        }
	}*/
    
	public float totalCost() {
		//Call getALuckyItem
		//DigitalVideoDisc luckyItemDigitalVideoDisc = getALuckyItem();
		//System.out.println(luckyItemDigitalVideoDisc.getCost());
		float sum = 0.0f;
		//boolean isUse = false;
		/*for (int i=0; i<qtyOrdered; i++ )  
			if (itemOrdered[i].equals(luckyItemDigitalVideoDisc) && !isUse)
			{
				isUse = true;
				continue;
			}
		else {
			sum = sum + itemOrdered[i].getCost();			
		}*/
		for (Media media: itemsOrdered) {
			sum += media.getCost();
		}
		return sum;
	}
	
	/*public DigitalVideoDisc getALuckyItem() {
	int max = qtyOrdered-1;
	int min = 0;
	int range = max - min + 1;
	int rand = (int) (Math.random() * range) + min;
	System.out.println("Index of lucky item:" + rand);
	return itemOrdered[rand];
	}*/
	
	public Media getALuckyIem() {
		Random random = new Random();
		int luckyNumber = random.nextInt(this.itemsOrdered.size());
		itemsOrdered.get(luckyNumber).setCost(0);
		return itemsOrdered.get(luckyNumber);
	}
	
	/*public void printing() {
		System.out.println("***************************************** ORDER *****************************************");
        System.out.println("Date: " + dateOrdered.getDay() + "/" + dateOrdered.getMonth() + "/" + dateOrdered.getYear() + "\nOrdered Items:");
        System.out.println("  Name - Title - Category - Director - Length : Price($)");
        //for(int i=0; i< qtyOrdered; i++) {
        //    System.out.println((i+1) + ".DVD - " + itemOrdered[i].getTittle() + " - " + itemOrdered[i].getCategory() + " - " + itemOrdered[i].getDirector()+ " - " + itemOrdered[i].getLength()+ " : " + itemOrdered[i].getCost() + "$");
        //}
        System.out.println("Total cost: " + totalCost());
        System.out.println("*****************************************************************************************");
    }*/
	//Handling potential Null Pointer Exception
    public void printAnOrder() throws NullPointerException {
        System.out.println("***********************Order***********************");
        MyDate dateOrdered = new MyDate();
        System.out.println();
        System.out.println("Date: " + dateOrdered.getLocalDate());
        System.out.println("Ordered Items:");

        for (Media item: itemsOrdered) {
            if (item instanceof DigitalVideoDisc) {
                DigitalVideoDisc disc = new DigitalVideoDisc((DigitalVideoDisc) item);  //down-casting
            System.out.println("DVD - [" + disc.getTitle() + "] - [" + disc.getCategory() + "] - [" + disc.getDirector() + "] - [" + disc.getLength() + "]: [" + disc.getCost() + " $]");
            } else {
                Book book = new Book((Book) item);                                      //down-casting
                System.out.println("--> Book: [" + book.getTitle() + "]");
                System.out.println(" <+> Category: [" + book.getCategory() + "]");
                System.out.print(" <+> Author(s):");
                
                for (String author: book.getAuthors()) {
                    System.out.print(" [" + author + "]");
                }
                System.out.println();
                System.out.println(" <+> Cost: [" + book.getCost() + " $]");
            }
            System.out.println();
        }
        System.out.println("Total cost: [" + this.totalCost() + " $]");
        System.out.println("***************************************************");
    }
	

	
}
