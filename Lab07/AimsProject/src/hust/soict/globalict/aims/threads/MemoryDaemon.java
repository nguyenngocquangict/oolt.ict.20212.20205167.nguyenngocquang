package hust.soict.globalict.aims.threads;

public class MemoryDaemon implements java.lang.Runnable{

	private long memoryUsed;
	public MemoryDaemon() {
		memoryUsed = 0;
	}
	public void run() {
		Runtime rtRuntime = Runtime.getRuntime();
		long used;
		
		while (true) {
			used = rtRuntime.totalMemory() - rtRuntime.freeMemory();
			if (used != memoryUsed) {
				System.out.println("\tMemory used = " + used);
				memoryUsed = used;
			}
		}
	}
	public long getMemoryUsed() {
		return this.memoryUsed;
	}
}
