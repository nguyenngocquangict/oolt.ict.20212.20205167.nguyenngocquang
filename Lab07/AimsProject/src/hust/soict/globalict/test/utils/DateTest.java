package hust.soict.globalict.test.utils;
import java.util.Scanner;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*//the current date
		MyDate date1 = new MyDate();
		date1.print();
		
		//the date with 3 parameters
		MyDate date2 = new MyDate(5, 5, 2022);
		date2.print();
		
		//the date with string parameters
		MyDate date3 = new MyDate("May 05 2022");
		date3.print();
		
		//the date inputed from the keyboard
		MyDate date4 = new MyDate().accept();
		date4.print();*/
		
		//Test print method
		MyDate date5 = new MyDate();
		date5.print();
		
		//Test another print method
		//MyDate date6 = new MyDate(3, 12, 2021);
		//date6.anotherPrintFormat();
		
		//Test DateUtils
		MyDate myList[] = new MyDate[10];
		
		myList[0] = new MyDate(4, 3, 2001);
		myList[1] = new MyDate(5, 6, 2003);
		myList[2] = new MyDate(12, 3, 2004);
		myList[3] = new MyDate(12, 3, 2000);
		myList[4] = new MyDate(7, 2, 2005);
		myList[5] = new MyDate(18, 3, 2000);
		myList[6] = new MyDate(19,2,2001);
		
		System.out.println("\n\nshow compare date: ");
		int check = DateUtils.compareDate(myList[0], myList[6]);
		if (check < 0) {
			myList[0].print();
			System.out.print(" begins earlier than ");
			myList[6].print();
		}
		else if (check > 0) {
			myList[0].print();
			System.out.print (" begins later than ");
			myList[6].print();
		}
		else System.out.println("These two days are the same");
			
		System.out.println("\n\nList of date after sorted: ");
		DateUtils.sort(myList, 7);
		for (int i=0; i<7; i++) {
			myList[i].print();
			System.out.print('\n');
		}
		
		
		//Test constructor (string day, string month, string year)
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the day in word:");
		String dayString = keyboard.nextLine();
		System.out.print("Enter the month in word:");
		String monthString = keyboard.nextLine();
		System.out.print("Enter the year in word:");
		String yearString = keyboard.nextLine();
		MyDate converteDate = new MyDate(dayString, monthString, yearString);
		System.out.println(converteDate.getDay() + "/" + converteDate.getMonth() + "/" + converteDate.getYear());
	}
	
	

}
