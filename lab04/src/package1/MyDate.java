import java.time.LocalDate;
import java.text.SimpleDateFormat;
import java.util.*;

public class MyDate {
    LocalDate local = LocalDate.now();

    private int day;
    private int month;
    private int year;


    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    //generate constructor no parameter
    public MyDate(){
        this.day = local.getDayOfMonth();
        this.month = local.getMonthValue();
        this.year = local.getYear();
    }

    //generate constructor with 3 parameters
    public MyDate(int ngay, int thang, int nam){
        this.day = ngay;
        this.month = thang;
        this.year = nam;
    }

    //generate constructor with string date .
    public MyDate(String str) throws Exception{
        SimpleDateFormat formatter = new SimpleDateFormat("MMMMM dd yyyy");
        Date date1 = formatter.parse(str);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        this.day = cal.get(cal.DAY_OF_MONTH);
        this.month = cal.get(cal.MONTH) + 1;
        this.year = cal.get(cal.YEAR);
    }

    public void accept() throws Exception{
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the date string following format MMMM dd yyyy : \n");
        String str = sc.nextLine();
        SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd yyyy");
        Date date1 = formatter.parse(str);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        this.day = cal.get(cal.DAY_OF_MONTH);
        this.month = cal.get(cal.MONTH) +1;
        this.year = cal.get(cal.YEAR);
    }

    public void print(){
        System.out.println("The current date is : ");
        System.out.println("Day: "+this.day + " Month: "+this.month + " Year:"+this.year);
        System.out.println("\n");
    }

    //generate constructor with 1 string parameter
    // public MyDate(String date) throws Exception{
    //     SimpleDateFormat formatter = new SimpleDateFormat("EEEEE, dd yyyy");
    //     Date date1 = formatter.parse(date);


    // }

    //create overloading constructor methods mydate(string day, string month)
    public MyDate(String Day,String Month,String Year) throws Exception{
		this.year = Integer.parseInt(Year);
		switch (Month) {
		case "January" :
			this.month = 1;
			break;
		case "February":
			this.month = 2;
			break;
		case "March" :
			this.month = 3;
			break;
		case "April":
			this.month = 4;
			break;
		case "May" :
			this.month = 5;
			break;
		case "June":
			this.month = 6;
			break;
		case "July" :
			this.month = 7;
			break;
		case "August":
			this.month = 8;
			break;
		case "September" :
			this.month = 9;
			break;
		case "October":
			this.month = 10;
			break;
		case "November" :
			this.month = 11;
			break;
		case "December":
			this.month = 12;
			break;
		default:
			System.out.println("error, the month should be write like : September");
			System.exit(11);
		}
		switch (Day) {
		case "first":
			this.day = 1;
		case "second":
			this.day = 2;
		case "third":
			this.day = 3;
		case "twenty-first":
			this.day = 21;
		case "twenty-second":
			this.day = 22;
		case "twenty-third":
			this.day = 23;
		case "thirty-first":
			this.day = 31;
		default:
			this.day = Integer.parseInt(Day);
		}
	}
	

}

