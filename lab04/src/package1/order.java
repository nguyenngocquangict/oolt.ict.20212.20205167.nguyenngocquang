import java.util.ArrayList;

public class order {
    //constant with method static
    public static final int MAX_NUMBERS_ORDERED = 10;

    //list 
    public DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    //the current number of quantity 
    private static int qtyOrdered = 0;

    //method to add more an item to the list
    private void addDigitalVideoDisc(DigitalVideoDisc disc){
        if(qtyOrdered <= MAX_NUMBERS_ORDERED){
            itemOrdered[qtyOrdered] = disc;
            qtyOrdered ++; 
            System.out.println("This disc has been added! \n");
        }
        else 
            System.out.println("The order is almost full. Cannot add items to order! \n");
    }

    //overloading method
    private void addDigitalVideoDisc(ArrayList<DigitalVideoDisc> dvdList){
         for(DigitalVideoDisc d : dvdList){
             if(qtyOrdered <= MAX_NUMBERS_ORDERED){
                 itemOrdered[qtyOrdered] = d;
                 qtyOrdered++;
             }
             else{
                 System.out.println("You cannot add item into the current order because it is full.!!\n");
             }
         }
    }

    private void removeDigitalVideoDisc(DigitalVideoDisc disc){
        for(int i = 0; i < qtyOrdered; i++){
            if( disc == itemOrdered[i]){
                for(int j = i; j < qtyOrdered; j++){
                    itemOrdered[j] = itemOrdered[j+1];
                }
                qtyOrdered --;

            }
        }
    }

    public double totalCost(){
        double sum = 0.0;
        for(int i = 0; i < qtyOrdered; i++){
            sum += itemOrdered[i].getCost();
        }

        return sum;
    }

    public static void main(String[] args){
        order anOrder = new order();

        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        anOrder.addDigitalVideoDisc(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star wars");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addDigitalVideoDisc(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdi");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);
        anOrder.addDigitalVideoDisc(dvd3);

        System.out.println("Total cost is: ");
        System.out.println(anOrder.totalCost());

        //write code to test the remove function of the order class
        anOrder.removeDigitalVideoDisc(dvd1);
        System.out.println("The rest disc in the order is :");
        System.out.println(qtyOrdered);

        System.out.println("List title of disc in order is :");
        //print all title of disc list
        for(int i = 0; i < qtyOrdered; i++){
            System.out.println((anOrder.itemOrdered)[i].getTitle());
        }

        ArrayList<DigitalVideoDisc> arrDigi = new ArrayList<>();
        arrDigi.add(dvd1);
        arrDigi.add(dvd2);
        arrDigi.add(dvd3);
        anOrder.addDigitalVideoDisc(arrDigi);

        for(int i = 0; i < qtyOrdered; i++){
            System.out.println((anOrder.itemOrdered)[i].getTitle());
        }

    }
    //use overloading method
    
}
