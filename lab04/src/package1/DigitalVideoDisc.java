public class DigitalVideoDisc{
    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    //constructor to create dvd object by title
    public DigitalVideoDisc(String title1){
        this.title = title1;

    }
    
    public DigitalVideoDisc(String title1, String category1){
        this.title = title1;
        this.category = category1;
    }

    public DigitalVideoDisc(String title1, String category1, String director1 ){
        this.title = title1;
        this.category = category1;
        this.director = director1;
    }

    public DigitalVideoDisc(String title1, String category1, String director1, int length1, float cost1 ){
        this.title = title1;
        this.category = category1;
        this.director = director1;
        this.length = length1;
        this.cost = cost1;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public String getDirector() {
        return director;
    }
    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }

    public float getCost() {
        return cost;
    }
    public void setCost(float cost) {
        this.cost = cost;
    }

     public static void main(String[] args) {
        //Create instance of class 
        //because all classes of java will herit from the java.lang.object so if a clas has no constructor 
        //method , this class in fact uses the constructor method of java.lang.object.
        //therefore we always create an instance of class by a no-arguments constructor method.
        //DigitalVideoDisc DVD = new DigitalVideoDisc("title 1 ");        
    }
}