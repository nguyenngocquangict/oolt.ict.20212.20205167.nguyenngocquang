
public class test {
    private int a;
    private int b;
    public test(int a, int b){
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }
    public void setA(int a) {
        this.a = a;
    }
    public int getB() {
        return b;
    }
    public void setB(int b) {
        this.b = b;
    }
    //test passing the parame
    public static void main(String[] args){
        // int a = 3, b = 4;
        // System.out.println("Before swap: a = "+a + " ;b = "+b);

        // t.swap(3,4);
        // System.out.println("After swap: a = "+a+"; b = "+b);

        test soA = new test(34, 25);
        test soB  = soA;//reference the same object in heap;
        //tức là soA va soB cùng tham chiếu tới cùng một object trong bộ nhớ heap, do đó
        //khi mà thay đổi qua soB thì đối tượng gốc của chúng ta cũng thay đổi theo.
        soB.setA(44);
        soB.setB(54);
        System.out.println("element a of soA is : "+soA.getA()+";, soB is :"+soA.getB());


    }
}
    

