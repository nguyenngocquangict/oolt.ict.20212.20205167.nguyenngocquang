
public class DateTest  {
    public static void main(String[] args) throws Exception{
        MyDate dateOne = new MyDate();
        dateOne.print();


        MyDate dateTwo = new MyDate(23, 2, 2022);
        dateTwo.print();


        MyDate dateThree = new MyDate("February 23 2019");
        dateThree.print();

//The current date is : 
// Day: 6 Month: 5 Year:2022


// The current date is : 
// Day: 23 Month: 2 Year:2022


// The current date is : 
// Day: 23 Month: 2 Year:2019

        MyDate dateFour =  new MyDate();
        dateFour.accept();
        dateFour.print();

        MyDate dateFifth = new MyDate("first","February", "twentynineteen");
        System.out.println("Date five has structure is : ");
        dateFifth.print();

    }
    
}
