import java.util.*;
import javax.swing.JOptionPane;
import java.lang.Math;

public class Equations{

   public static void main(String args[]){
        double a, b, c, a11, a12, a21, a22, delta, b1, b2;
        int selection ;

        Scanner scan = new Scanner(System.in);
        
        System.out.println("1. The first-degree equation with one variable\n" +
                        "2. The first-degree equation with two variables\n" +
                        "3. The second-degree equation with one variable\n" +
                        "Select:");
        selection  = scan.nextInt();
        
        switch (selection) {
            case 1:
                System.out.println("The form of input:\n ax + b = 0  (a is not equal to 0)\n");
                System.out.println("The value of  a is : ");
                a = scan.nextDouble();
                
                if(a !=0){
                    b = scan.nextDouble();
                    System.out.println("The result of equation is : x = " + (-b/a));
                    System.out.println("The solution: x= " + (-b/a));
                }
                else 
                    System.out.println("The value of must not be 0\n");
                    System.out.println("The equation has no solution");

                break;

            case 2:
                System.out.println("The form of input:\n a_11 x_1 + a_12 x_2 = b_1\n a_21 x_1 + a_22 x_2 = b_2\nThe value of  a_11 is :");
                a11 = scan.nextDouble();
                
                System.out.println("The value of a_12 is : ");
                a12 = scan.nextDouble();

                System.out.println("The value of a_21 is :");
                a21 = scan.nextDouble();

                
                System.out.println("The value of a_22 is :");
                a22 = scan.nextDouble();
                

                System.out.println("The value of b_1 is : ");
                b = scan.nextDouble();

                System.out.println("The value of b_2 is : ");
                c = scan.nextDouble();
                
                
                a = (a11 * a22 - a21 * a12); 

                if(a != 0){
                    System.out.println("The solution of equation is : \n");
                    System.out.println("x1 = "+ (b * a22 - c *a12)/a + "x2 = "+ (c * a11 - b * a21)/ a);         
                }
                
                else 
                    System.out.println("The system equation has no solution \n");

                break;

            case 3:

                System.out.println("The form of input:\n ax^2 + bx + c = 0\n");
                System.out.println("The value of a is : ");
                a = scan.nextDouble();
                
                System.out.println("The value of b is : ");
                b = scan.nextDouble();
                
                System.out.println("The value of c is : ");
                c = scan.nextDouble();

                delta = Math.pow(b, 2) - 4 * a * c;

                if(delta > 0){
                    System.out.println("The equation has two distinct roots : \n");
                    System.out.println("x1 = " + (-b - Math.sqrt(delta) / (2 * a) ));
                    System.out.println("x2 = " + (-b + Math.sqrt(delta) / (2 * a) ));

                }

                else if(delta < 0 ){
                    
                    System.out.println("The equation has no solution \n");
                }

                else 
                    System.out.println("The equation has double root x = " + (-b / 2 * a));

                break;
            default:
                System.out.println("Invalid selection \n");

                
                break;
        }
   } 
}