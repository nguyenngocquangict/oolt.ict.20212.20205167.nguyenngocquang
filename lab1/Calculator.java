import javax.print.attribute.standard.JobHoldUntil;
import javax.swing.JOptionPane;
import java.math.*;

public class Calculator{
    public static void main(String[] args){

        String strA, strB;
        double a, b,  sum, dif, product, quotient;

        strA = JOptionPane.showInputDialog(null, "Input the value of a : ", "Input a : ", JOptionPane.INFORMATION_MESSAGE);
        a = Double.parseDouble(strA);

        strB = JOptionPane.showInputDialog(null, "Input the value of b : ", "Input b : ", JOptionPane.INFORMATION_MESSAGE);
        b = Double.parseDouble(strA);

        JOptionPane.showMessageDialog(null, "Sum of a and b is : " + (a+b));
        JOptionPane.showMessageDialog(null, "Different of a and b is : "+ Math.abs(a-b) + "\n");
        JOptionPane.showMessageDialog(null, "Product of a and b is : " + (a*b) + "\n");
        if(b == 0 )
            JOptionPane.showMessageDialog(null, "Invalid (The value of b must not be 0) \n");
        else 
            JOptionPane.showMessageDialog(null, "Quotient of a and b is : " + (a/b) + "\n");


    }
}