import java.util.Scanner;

public class Array {
    public static void main(String[] args) 
    {
        int n, sum = 0;
        float average;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter no. of elements you want in array:");
        n = s.nextInt();
        int a[] = new int[n];
        System.out.println("Enter all the elements:");
        for(int i = 0; i < n ; i++)
        {
            a[i] = s.nextInt();
            sum = sum + a[i];
        }
        System.out.println("Sum:"+sum);
        average = (float)sum / n;
        System.out.println("Average:"+average);

        //sort of array
        for(int i = 0; i < n; i++){
            for(int j = i; j < n; j++){
                if(a[i] >  a[j]){
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;

                }
            }
        }

        System.out.println("Array after be sorted is :");
        for(int i = 0; i < n; i++){
            System.out.println(a[i]);
        }
    }
    // Enter no. of elements you want in array:6
    // Enter all the elements:
    // 23
    // 25
    // 11
    // 23
    // 113
    // 2
    // Sum:197
    // Average:32.833332
    // Array after be sorted is :
    // 2
    // 11
    // 23
    // 23
    // 25
    // 113

}
