import java.util.Scanner;

public class InputFromKeyboard{
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);

        System.out.println("What is your name?");
        String strName = keyboard.nextLine();
        System.out.println("How old are you?");
        int iAge  = keyboard.nextInt();
        System.out.println("How tall are you?");
        double dHeight = keyboard.nextDouble();

        System.out.println("Mrs/Ms. "+ strName+ "," + iAge + "years old"+  "Your height is : "+ dHeight);
    
    }
// What is your name?
// quang
// How old are you?
// 20
// How tall are you?
// 123
// Mrs/Ms. quang,20years oldYour height is : 123.0

}